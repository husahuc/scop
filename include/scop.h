#ifndef __SCOP_H
#define __SCOP_H

#ifdef __APPLE__
#define GL_SILENCE_DEPRECATION
#endif


#include "../lib/glfw-3.3.2.bin.MACOS/include/GLFW/glfw3.h"
#include "../lib/mini_libft/libft.h"
#include <stdio.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <math.h>

# define FT_PI 3.14159265358979323846264338327950288

#define SCR_WIDTH 1200
#define SCR_HEIGHT 1000

#define TRUE 1
#define FALSE 0

#define IS_MOVING 1
#define NOT_MOVING 0

#define DIRECTION_Y 0
#define DIRECTION_X 1

#define CYLINDER_MAPPING_ON 1
#define CYLINDER_MAPPING_OFF 0

#define MOVE_X_OBJ 2
#define MOVE_Y_OBJ 0
#define MOVE_Z_OBJ 1

typedef struct      s_obj
{
    char        *path;
    float       *vertices;
    float       *uv;
    int         nb_faces;
}                   t_obj;

typedef struct      s_glfw
{
    unsigned int    VBO;
    unsigned int    cubeVAO;
    int             shaderProgram;
    float           projection[16];
    unsigned int    projectionLoc;
    int             win_w;
    int             win_h;
    GLFWwindow*     window;
    GLenum          mode;
}                   t_glfw;

typedef struct      s_texture
{
    GLuint          texture;
    char            *path;
    int             size;
    int             width;
    int             height;
    int             sl;
    unsigned char   *img;
}                   t_texture;

typedef struct      s_scop
{
    float       rotationx;
    float       rotationy;
    float       centerheight;
    float       radius;
    int         is_texture;
    int         cylinder_mapping;
    int         direction_mapping;
    int         infinite_turn;
    double      current_time;
    double      last_time;
    int         nb_frames;
    float       fov;
    char        *fragment_shader_path;
    char        *vertex_shader_path;
    t_obj       *obj;
    t_glfw      *glfw;
    t_texture   *texture;
}                   t_scop;

typedef struct      s_vertex
{
    float       x;
    float       y;
    float       z;
}                   t_vertex;

/*
** Cli
*/
void		ft_explain_keys(void);
t_scop		*ft_cli_scop(int argc, char **argv);

/*
** Shaders
*/
int         ft_loadshader(char *file, GLenum shaderType);
void	    shader_program(t_scop *scop);

/*
** Object
*/
int		    ft_loadobj(t_scop *scop);
int         ft_move_obj(t_scop *scop, int type, float move);
int         ft_center_obj(t_scop *scop);


/*
** Texture
*/
int		    ft_load_BMP(t_scop *scop);
void		ft_load_texture(t_scop *scop);

/*
** Matrix
*/
float       *init_identity_matrix_4(float *mat4);
float       *translate_matrix_4(float *mat4, float vec3[3]);
float       *scale_matrix_4(float *mat4, float vec3[3]);
float       *rotate_matrix_4(float *mat4, float angle, float vec3[3]);
void        ft_look_at_matrix(float *mat4, float *eye, float *center, float *up);
float       *projection_matrix_4(float *mat4, float fov, float aspect, float NearVal, float farVal);

void        processInput(GLFWwindow *window);
void        framebuffer_size_callback(GLFWwindow* window, int width, int height);

/*
** Glfw
*/
void        ft_init_glfw(t_scop *scop);
void        ft_loop_glfw(t_scop *scop);
void        ft_end_glfw(t_scop *scop);
float       ft_degree(float rad);

/*
**Vector
*/
void       ft_vector_normalize_in(float *vec3);
float       *ft_vector_normalize_out(float *vec3);
float       ft_vector_dot(float *vec3_1, float *vec3_2);
float       *ft_vector_sub(float *vec3_1, float *vec3_2);
float       *ft_vector_add(float *vec3_1, float *vec3_2);
float       *ft_vector_cross(float *vec3_1, float *vec3_2);
float		*ft_camera_pos(t_scop *scop);

/*
** Callback keyboard
*/
void        KeyboardCallback(GLFWwindow* window, int key, int scancode, int action, int mods);

#endif
