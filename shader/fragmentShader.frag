#version 330 core
out vec4 FragColor;

uniform sampler2D texture1;
uniform int istexture;
in vec2 UV;
in float face;


void main()
{
    if (istexture == 0)
        FragColor = texture(texture1, UV);
    else
        FragColor = vec4(mod(face, 6) / 10, mod(face, 6) / 10, mod(face, 6) / 10, 1.0f);
}