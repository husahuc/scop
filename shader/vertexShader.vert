#version 330 core
layout (location = 0) in vec4 aPos;

# define FT_PI 3.14159265358979323846264338327950288

out float face;
out vec2 UV;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform int cylinder_mapping;
uniform int direction_mapping;

vec2 cylinder_texture_mapping(vec3 position)
{
	float a;
	float b;

	a = 0.5 + atan(position.x, position.z) / FT_PI * 0.5;

	b = position.y / 10.0;
	return (vec2(a, b) * 15.0);
}

void main()
{
    face = aPos.w;
    gl_Position = projection * view * model * vec4(vec3(aPos), 1.0);
	if (cylinder_mapping == 0)
		if (direction_mapping == 0)
			UV = vec2(aPos.x*2, aPos.y*2);
		else
			UV = vec2(aPos.z*2, aPos.y*2);
	else
    	UV = cylinder_texture_mapping(vec3(aPos));
}