#include "../include/scop.h"

int     ft_error_shader(int error)
{
    if (error == 1)
        ft_mini_printf("Could not open file shader\n");
    return (-1);
}

void    shader_error(int shader)
{
    int success;
    char infoLog[512];

    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(shader , 512, NULL, infoLog);
        ft_mini_printf("COMPILATION_FAILED %s\n", infoLog);
        ft_mini_printf("Supported GLSL version is %s.\n", (char *)glGetString(GL_SHADING_LANGUAGE_VERSION));
        exit(0);
    }
}

int     ft_loadshader(char *file, GLenum shaderType)
{
    char  *shader_source;
    int         Shader;    
    int         size;
    int         read_size;
    FILE        *file_handler;
    
    file_handler = fopen(file, "r");
    if (!file_handler)
        return (ft_error_shader(1));
    fseek(file_handler, 0, SEEK_END);
    size = ftell(file_handler);
    rewind(file_handler);
    shader_source = malloc(sizeof(char) * (size + 1));
    if (!shader_source)
        return (ft_error_shader(1));
    read_size = fread(shader_source, sizeof(char), size, file_handler);
    shader_source[size] = '\0';
    if (size != read_size)
    {
        free(shader_source);
        return (ft_error_shader(1));
    }
    fclose(file_handler);

    Shader = glCreateShader(shaderType);
    glShaderSource(Shader, 1, (const GLchar *const *)&shader_source, NULL);
    free(shader_source);
    glCompileShader(Shader);
    shader_error(Shader);
    return(Shader);
}