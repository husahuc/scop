#include "../include/scop.h"

int     ft_error_glfw(int error)
{
    if (error == 1)
        ft_mini_printf("Error GLFW init\n");
	if (error == 1)
        ft_mini_printf("Error GLFW window\n");
    exit(-1);
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	(void)window;
	glViewport(0, 0, width, height);
}

void		ft_print_glfw(t_scop *scop)
{
	float model[16];
	float view[16];

	init_identity_matrix_4(model);

	rotate_matrix_4(model, ft_degree(-55.0f), (float[3]){1.0f, 0.0f, 0.0f});
	init_identity_matrix_4(model);

	float cameraUp[3] = {0.0, 1.0, 0.0};
	float center[3] = {0.0, scop->centerheight, 0.0};

	float *cameraPos = ft_camera_pos(scop);
	init_identity_matrix_4(view);
	ft_look_at_matrix(view, cameraPos, center, cameraUp);
	free(cameraPos);

	unsigned int modelLoc = glGetUniformLocation(scop->glfw->shaderProgram, "model");
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, model);
	
	unsigned int viewLoc  = glGetUniformLocation(scop->glfw->shaderProgram, "view");
	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, view);
	
	unsigned int isTextureLoc  = glGetUniformLocation(scop->glfw->shaderProgram, "istexture");
	glUniform1i(isTextureLoc, scop->is_texture);

	unsigned int cylinder_mappingLoc  = glGetUniformLocation(scop->glfw->shaderProgram, "cylinder_mapping");
	glUniform1i(cylinder_mappingLoc, scop->cylinder_mapping);

	unsigned int direction_mappingLoc  = glGetUniformLocation(scop->glfw->shaderProgram, "direction_mapping");
	glUniform1i(direction_mappingLoc, scop->direction_mapping);

	glPolygonMode(GL_FRONT_AND_BACK, scop->glfw->mode);

	glBindBuffer(GL_ARRAY_BUFFER, scop->glfw->VBO);
	glBufferData(GL_ARRAY_BUFFER, scop->obj->nb_faces * 4 * 3 * sizeof(float), scop->obj->vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	glDrawArrays(GL_TRIANGLES, 0, scop->obj->nb_faces*3);
	glfwSwapBuffers(scop->glfw->window);
}

void         ft_loop_glfw(t_scop *scop)
{
	scop->current_time = glfwGetTime();
	scop->nb_frames++;
	if (scop->current_time - scop->last_time >= 1.0)
	{
		ft_mini_printf("%d fps\r", scop->nb_frames);
		scop->nb_frames = 0;
		scop->last_time += 1.0;
	}
	processInput(scop->glfw->window);

	scop = glfwGetWindowUserPointer(scop->glfw->window);

	glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	projection_matrix_4(scop->glfw->projection, ft_degree(scop->fov), scop->glfw->win_w / scop->glfw->win_h , 0.1f, 100.0f);

	scop->glfw->projectionLoc  = glGetUniformLocation(scop->glfw->shaderProgram, "projection");
	glUniformMatrix4fv(scop->glfw->projectionLoc, 1, GL_FALSE, scop->glfw->projection);

	glUseProgram(scop->glfw->shaderProgram);

	if (scop->infinite_turn == IS_MOVING)
		scop->rotationx += 0.01;
	ft_print_glfw(scop);
	glfwPollEvents();
}

void		ft_end_glfw(t_scop *scop)
{
	glDeleteBuffers(1, &scop->glfw->VBO);
	glDeleteVertexArrays(1, &scop->glfw->cubeVAO);
	glDeleteTextures(1, &scop->texture->texture);
	glDeleteProgram(scop->glfw->shaderProgram);

	glfwTerminate();
    free(scop->obj);
    free(scop->glfw);
	free(scop->texture);
    free(scop);
	exit(0);
}

void		ft_init_glfw(t_scop *scop)
{
    GLFWwindow *window;

    if (!glfwInit())
	{
		ft_error_glfw(1);
		return ;
	}

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

    window = glfwCreateWindow(scop->glfw->win_w, scop->glfw->win_h, "Hello World", NULL, NULL);
    if (!window)
    {
		ft_error_glfw(2);		
        glfwTerminate();
        return ;
    }
    glfwMakeContextCurrent(window);
    scop->glfw->window = window;
    glfwSetWindowUserPointer(scop->glfw->window, scop);

    scop->glfw->shaderProgram = glCreateProgram();

	shader_program(scop);
	glEnable(GL_DEPTH_TEST);

	glGenVertexArrays(1, &scop->glfw->cubeVAO);
	glGenBuffers(1, &scop->glfw->VBO);

	glBindVertexArray(scop->glfw->cubeVAO);
	glBindBuffer(GL_ARRAY_BUFFER, scop->glfw->VBO);
	glBufferData(GL_ARRAY_BUFFER, scop->obj->nb_faces * 4 * 3 * sizeof(float), scop->obj->vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	glUseProgram(scop->glfw->shaderProgram);
	glUniform1i(glGetUniformLocation(scop->glfw->shaderProgram, "texture"), 0);

	projection_matrix_4(scop->glfw->projection, ft_degree(scop->fov), scop->glfw->win_w / scop->glfw->win_h , 0.1f, 100.0f);

	scop->glfw->projectionLoc  = glGetUniformLocation(scop->glfw->shaderProgram, "projection");
	glUniformMatrix4fv(scop->glfw->projectionLoc, 1, GL_FALSE, scop->glfw->projection);

	glfwSetKeyCallback(scop->glfw->window, KeyboardCallback);
	ft_load_texture(scop);

	scop->last_time = glfwGetTime();
	scop->nb_frames = 0;
}