#include "../include/scop.h"

int main(int argc, char **argv)
{
	t_scop *scop;
	scop = ft_cli_scop(argc, argv);

	ft_init_glfw(scop);

	while (!glfwWindowShouldClose(scop->glfw->window))
	{
		ft_loop_glfw(scop);
	}
	ft_end_glfw(scop);
	return 0;
}