#include "../include/scop.h"

void       ft_vector_normalize_in(float *vec3)
{
	float length;

	length = sqrt(vec3[0] * vec3[0] + vec3[1] * vec3[1] + vec3[2] * vec3[2]);
	vec3[0] /= length;
	vec3[1] /= length;
	vec3[2] /= length;
}

float       *ft_vector_normalize_out(float *vec3)
{
	float length;
	float *ret;

	ret = malloc(3 * sizeof(float));
	length = sqrt(vec3[0] * vec3[0] + vec3[1] * vec3[1] + vec3[2] * vec3[2]);
	vec3[0] /= length;
	vec3[1] /= length;
	vec3[2] /= length;
	return(ret);
}

 float       ft_vector_dot(float *vec3_1, float *vec3_2)
{
	return vec3_1[0] * vec3_2[0] + vec3_1[1] * vec3_1[1] + vec3_1[2] * vec3_2[2];
}

float       *ft_vector_sub(float *vec3_1, float *vec3_2)
{
	float *ret;

	ret = malloc(3 * sizeof(float));
	ret[0] = vec3_1[0] - vec3_2[0];
	ret[1] = vec3_1[1] - vec3_2[1];
	ret[2] = vec3_1[2] - vec3_2[2];
	return ret;
}

float       *ft_vector_add(float *vec3_1, float *vec3_2)
{
	float *ret;

	ret = malloc(3 * sizeof(float));
	ret[0] = vec3_1[0] + vec3_2[0];
	ret[1] = vec3_1[1] + vec3_2[1];
	ret[2] = vec3_1[2] + vec3_2[2];
	return ret;
}

float       *ft_vector_cross(float *vec3_1, float *vec3_2)
{
	float *ret;
	ret = malloc(3 * sizeof(float));
	ret[0] = vec3_1[1] * vec3_2[2] - vec3_1[2] * vec3_2[1];
	ret[1] = vec3_1[2] * vec3_2[0] - vec3_1[0] * vec3_2[2];
	ret[2] = vec3_1[0] * vec3_2[1] - vec3_1[1] * vec3_2[0];
	return ret;
}

float       ft_degree(float rad)
{
	return (rad * FT_PI / 180.0f);
}

float		*ft_camera_pos(t_scop *scop)
{
	float *cameraPos;
	cameraPos = malloc(3 * sizeof(float));

	cameraPos[0] = sin(scop->rotationx) * scop->radius;
	cameraPos[1] = scop->rotationy * scop->radius;
	cameraPos[2] = cos(scop->rotationx) * scop->radius;

	return cameraPos;
}