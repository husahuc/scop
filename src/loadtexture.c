#include "../include/scop.h"

int     ft_error_texture(int error)
{
    if (error == 1)
        ft_putendl("Could not open file texture");
	if (error == 2)
        ft_putendl("Not a BMP file");
	return (0);
}

int			ft_read_header(t_scop *scop)
{
	FILE		*file;
	short		opp;
	short		bpp;

	if ((file = fopen(scop->texture->path, "r")) == NULL)
	{
		ft_error_texture(1);
		return (-1);
	}

	fseek(file, 18, SEEK_SET);
	fread(&scop->texture->width, 4, 1, file);
	fread(&scop->texture->height, 4, 1, file);
	fseek(file, 2, SEEK_CUR);
	fread(&bpp, 2, 1, file);
	fclose(file);
	opp = bpp / 8;
	scop->texture->sl = scop->texture->width * opp;

	scop->texture->width < 0 ? scop->texture->width = -scop->texture->width : 0;
	scop->texture->height < 0 ? scop->texture->height = -scop->texture->height : 0;
	scop->texture->size = scop->texture->sl * scop->texture->height;
	return (0);
}

void			ft_get_image(t_scop *scop)
{
	int		fd;
	char	*buffer;
	int		size = scop->texture->size * 2;
	int		i;
	int		j;
	int		h = 0;

	buffer = (char *)malloc(sizeof(char) * scop->texture->size + 1);
	fd = open(scop->texture->path, O_RDONLY);
	lseek(fd, 54, SEEK_SET);
	i = read(fd, buffer, scop->texture->size);

	scop->texture->img = (unsigned char *)malloc(sizeof(unsigned char) * size);
	while (i > 0)
	{
		i-= scop->texture->sl;
		j = 0;
		while (j < scop->texture->sl)
		{
			scop->texture->img[h + j] = (unsigned char)buffer[i + j + 2];
			scop->texture->img[h + j + 1] = (unsigned char)buffer[i + j + 1];
			scop->texture->img[h + j + 2] = (unsigned char)buffer[i + j];
			j+= 3;
		}
		h += scop->texture->sl;
	}
	free(buffer);

	close(fd);
}

int		ft_load_BMP(t_scop *scop)
{
	if (ft_read_header(scop) < 0)
		return (-1);
	ft_get_image(scop);
	return (0);
}

void	ft_load_texture(t_scop *scop)
{
	GLuint texture;

	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, scop->texture->width, scop->texture->height, 0, GL_RGB, GL_UNSIGNED_BYTE, scop->texture->img);

    scop->texture->texture = texture;
}