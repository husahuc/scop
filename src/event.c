#include "../include/scop.h"

void KeyboardCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    t_scop *scop;
    (void)scancode;
    (void)mods;

    scop = glfwGetWindowUserPointer(window);
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, TRUE);
    else if (key == GLFW_KEY_T && action == GLFW_PRESS)
        scop->is_texture = !scop->is_texture;
    else if (key == GLFW_KEY_R && action == GLFW_PRESS)
        scop->infinite_turn = !scop->infinite_turn;
    else if (key == GLFW_KEY_C && action == GLFW_PRESS)
        scop->cylinder_mapping = !scop->cylinder_mapping;
    else if (key == GLFW_KEY_D && action == GLFW_PRESS)
        scop->direction_mapping = !scop->direction_mapping;
    else if (key == GLFW_KEY_0 && action == GLFW_PRESS)
        ft_center_obj(scop);
    else if (key == GLFW_KEY_W && action == GLFW_PRESS)
        scop->glfw->mode = (scop->glfw->mode == GL_FILL) ? GL_LINE: GL_FILL;
}

void    processInput(GLFWwindow *window)
{
    t_scop *scop;

    scop = glfwGetWindowUserPointer(window);

	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS && \
		(glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_RIGHT_SHIFT) == GLFW_PRESS))
		ft_move_obj(scop, MOVE_X_OBJ, -0.01);
    else if(glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
        scop->rotationx += 0.1;

	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS && \
		(glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_RIGHT_SHIFT) == GLFW_PRESS))
		ft_move_obj(scop, MOVE_X_OBJ, +0.01);
	else if(glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
        scop->rotationx -= 0.1;

    if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS && \
		(glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_RIGHT_SHIFT) == GLFW_PRESS))
		ft_move_obj(scop, MOVE_Y_OBJ, +0.01);
	else if(glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS && glfwGetKey(window, GLFW_KEY_O) == GLFW_PRESS)
        scop->centerheight += 0.1;
	else if(glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
        scop->rotationy += 0.1;

	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS && \
		(glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_RIGHT_SHIFT) == GLFW_PRESS))
		ft_move_obj(scop, MOVE_Y_OBJ, -0.01);
    else if(glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS && glfwGetKey(window, GLFW_KEY_O) == GLFW_PRESS)
        scop->centerheight -= 0.1;
    else if(glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
        scop->rotationy -= 0.1;

	if (glfwGetKey(window, GLFW_KEY_KP_ADD) == GLFW_PRESS && \
		(glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_RIGHT_SHIFT) == GLFW_PRESS))
		ft_move_obj(scop, MOVE_Z_OBJ, -0.01);
    else if (glfwGetKey(window, GLFW_KEY_KP_ADD) == GLFW_PRESS && glfwGetKey(window, GLFW_KEY_F) == GLFW_PRESS)
    {
        if (scop->fov >= 1.0 && scop->fov < 180.0)
            scop->fov += 1;
    }
    else if(glfwGetKey(window, GLFW_KEY_KP_ADD) == GLFW_PRESS)
        scop->radius += 0.1;

    if (glfwGetKey(window, GLFW_KEY_KP_SUBTRACT) == GLFW_PRESS && \
		(glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_RIGHT_SHIFT) == GLFW_PRESS))
		ft_move_obj(scop, MOVE_Z_OBJ, 0.01);
    else if (glfwGetKey(window, GLFW_KEY_KP_SUBTRACT) == GLFW_PRESS && glfwGetKey(window, GLFW_KEY_F) == GLFW_PRESS)
    {
        if (scop->fov > 1.0 && scop->fov < 180.9)
            scop->fov -= 1;
    }
    else if (glfwGetKey(window, GLFW_KEY_KP_SUBTRACT) == GLFW_PRESS)
        scop->radius -= 0.1;
}