#include "../include/scop.h"

int     ft_move_obj(t_scop *scop, int type, float move)
{
    int i = 0;

    while (i < (scop->obj->nb_faces * 3))
    {
        if (type == MOVE_X_OBJ)
        {
            scop->obj->vertices[i * 4] += move;
        }
        if (type == MOVE_Z_OBJ)
        {
            scop->obj->vertices[i * 4 + 1] += move;
        }
        if (type == MOVE_Y_OBJ)
        {
            scop->obj->vertices[i * 4 + 2] += move;
        }
        i++;
    }

    return (0);
}

int     ft_center_obj(t_scop *scop)
{
    float max_x = scop->obj->vertices[0];
    float min_x = scop->obj->vertices[0];
    float max_z = scop->obj->vertices[1];
    float min_z = scop->obj->vertices[1];
    float max_y = scop->obj->vertices[2];
    float min_y = scop->obj->vertices[2];
    int i = 0;

    while (i < (scop->obj->nb_faces * 3))
    {
        if (scop->obj->vertices[i * 4] > max_x)
            max_x = scop->obj->vertices[i * 4];
        if (scop->obj->vertices[i * 4] < min_x)
            min_x = scop->obj->vertices[i * 4];
        if (scop->obj->vertices[i * 4 + 1] > max_z)
            max_z = scop->obj->vertices[i * 4 + 1];
        if (scop->obj->vertices[i * 4 + 1] < min_z)
            min_z = scop->obj->vertices[i * 4 + 1];
        if (scop->obj->vertices[i * 4 + 2] > max_y)
            max_y = scop->obj->vertices[i * 4 + 2];
        if (scop->obj->vertices[i * 4 + 2] < min_y)
            min_y = scop->obj->vertices[i * 4 + 2];
        i++;
    }

    float inter_x = (min_x + max_x) / 2;
    float inter_y = (min_y + max_y) / 2;
    float inter_z = (min_z + max_z) / 2;
    i = 0;
    while (i < (scop->obj->nb_faces * 3))
    {
        scop->obj->vertices[i * 4] -= inter_x;
        scop->obj->vertices[i * 4 + 1] -= inter_z;
        scop->obj->vertices[i * 4 + 2] -= inter_y;
        i++;
    }
    scop->rotationx = 0.0;
    return (0);
}