#include "../include/scop.h"

void		ft_explain_keys(void)
{
	ft_putendl("Scop: ./scop obj_file[opional] texture_file.bmp[optional]");
	ft_putendl("in app:");
	ft_putendl("  move camera: up | down | right | left | + | -");
	ft_putendl("  move obj (texture not applying to the move): shift + up | down | right | left | + | -");
	ft_putendl("  move focus camera: o + up | down");
	ft_putendl("  set fov: f + = | +");
	ft_putendl("  set infinite rotation: r");
	ft_putendl("  set texture: t");
	ft_putendl("  center object: 0");
	ft_putendl("  set cylinder mapping for the texture: c");
	ft_putendl("  set direction mapping for the texture: d");
	ft_putendl("  set wireframe mode: w");
	ft_putendl("  exit: escape");
}

t_scop		*ft_cli_scop(int argc, char **argv)
{
	t_scop *scop;

    scop = malloc(sizeof(t_scop));
    scop->rotationx = 0.0f;
    scop->rotationy = 0.0f;
	scop->centerheight = 0.0f;
    scop->radius = 5.0f;
	scop->is_texture = TRUE;
	scop->infinite_turn = NOT_MOVING;
	scop->cylinder_mapping = CYLINDER_MAPPING_OFF;
	scop->direction_mapping = DIRECTION_Y;
	scop->fov = 45;
	scop->vertex_shader_path = "../shader/vertexShader.vert";
	scop->fragment_shader_path = "../shader/fragmentShader.frag";

	scop->obj = malloc(sizeof(t_obj));
	scop->obj->nb_faces = 0;

	scop->glfw = malloc(sizeof(t_glfw));
	scop->glfw->mode = GL_FILL;
	scop->glfw->win_w = 800;
	scop->glfw->win_h = 600;

	scop->texture = malloc(sizeof(t_texture));

	if (argc > 1)
	{
		if (ft_strcmp(argv[1], "--help") == 0)
		{
			ft_explain_keys();
			exit(0);
		}
		else
			scop->obj->path = argv[1];
	}
	else
		scop->obj->path = ft_strdup("resources/object/42.obj");
	if (argc > 2)
		scop->texture->path = argv[2];
	else
		scop->texture->path = "resources/texture/chaton.bmp";
	if ((ft_loadobj(scop)) == -1)
	{
		exit(-1);
	}
	ft_center_obj(scop);
	ft_load_BMP(scop);

    return scop;
}