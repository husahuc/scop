#include "../include/scop.h"

float       *init_identity_matrix_4(float *mat4)
{
    int     i;
    int     j;

    i = 0;
    while (i < 4)
    {
        j = 0;
        while (j < 4)
        {
            if (i == j)
                mat4[i*4+j] = 1;
            else
                mat4[i*4+j] = 0;
            j++;
        }
        i++;
    }

    return (mat4);
}

float       *multiply_matrix_4(float *mat4_1, float *mat4_2)
{
    int i;
    int j;
    int k;
    float sum;

    i = 0;
    while (i < 4)
    {
        j = 0;
        while (j < 4)
        {
            k = 0;
            sum = 0.0f;
            while (k < 4)
            {
                sum += mat4_1[i+k*4] * mat4_2[k+j*4];
                k++;
            }
            mat4_1[j*4+i] = sum;
            j++;
        }
        i++;
    }
    return mat4_1;
}

float       *scale_matrix_4(float *mat4, float vec3[3])
{

    float scale[16] = {
        vec3[0], 0, 0, 0,
        0, vec3[1], 0, 0,
        0, 0, vec3[2], 0,
        0, 0, 0, 1
    };

    multiply_matrix_4(mat4, scale);

    return mat4;
}

float       *translate_matrix_4(float *mat4, float vec3[3])
{
    float translate[16] = {
        1, 0, 0, 0,
        0, 1, 0, 0,
        0, 0, 1, 0,
        vec3[0], vec3[1], vec3[2], 1
    };

    multiply_matrix_4(mat4, translate);

    return mat4;
}

float       *rotate_matrix_4(float *mat4, float angle, float vec3[3])
{
    float cosang = cos(angle);

    float rotation2[16] = {
        1         , 0    , 0, 0,
        0   , cosang + pow(vec3[1], 2) * (1 - cosang), vec3[1]*vec3[2]*(1-cosang)+vec3[0]*sin(angle), 0,
        0   , vec3[1]*vec3[2]*(1-cosang)+vec3[0]*sin(angle) , cosang + pow(vec3[2], 2) * (1 - cosang),       0,
        0   ,0  ,0  ,1,
    };

    multiply_matrix_4(mat4, rotation2);

    return mat4;
}

/*
** (1 / tan(fov/2)) / aspect    0                   0                                                   0
** 0                            1.0f / tan(fov/2)   0                                                   0
** 0                            0                   -((farVal + NearVal) / (farVal - NearVal))          -1
** 0                            0                   2.0f * NearVal * farVal * 1.0f / (NearVal - farVal) -1
*/

float       *projection_matrix_4(float *mat4, float fov, float aspect, float NearVal, float farVal)
{
    mat4[0] = (1.0f / tan(fov/2)) / aspect;
    mat4[5] = 1.0f / tan(fov/2);
    mat4[10] = -((farVal + NearVal) / (farVal - NearVal));
    mat4[11] = -1;
    mat4[14] = 2.0f * NearVal * farVal * 1.0f / (NearVal - farVal);
    mat4[15] = 0;

    return (mat4);
}

float       ft_vec3_dot(float *a, float *b)
{
    return a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
}

/*
**  (Right)x    (Right)y    (Right)z    0
**  (Up)x       (Up)y       (Up)z       0
**  (forward)x  (Forward)y  (Forward)z  0
**  (T)x        (T)y        (Up)z       1
** https://www.scratchapixel.com/lessons/mathematics-physics-for-computer-graphics/lookat-function
*/

void        ft_look_at_matrix(float *mat4, float *eye, float *center, float *up)
{
    float *f;
    float *u;
    float *s;

    f = ft_vector_sub(center, eye);
    ft_vector_normalize_in(f);
    s = ft_vector_cross(f, up);
    ft_vector_normalize_in(s);
    u = ft_vector_cross(s, f);

    mat4[0] = s[0];
    mat4[1] = u[0];
    mat4[2] = -f[0];
    mat4[4] = s[1];
    mat4[5] = u[1];
    mat4[6] =-f[1];
    mat4[8] = s[2];
    mat4[9] = u[2];
    mat4[10] = -f[2];
    mat4[12] = -ft_vec3_dot(s, eye);
    mat4[13] = -ft_vec3_dot(u, eye);
    mat4[14] = ft_vec3_dot(f, eye);
    free(f);
    free(u);
    free(s);
}
