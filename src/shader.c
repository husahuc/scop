#include "../include/scop.h"

void	shader_program(t_scop *scop)
{
	int vertexShader = ft_loadshader(scop->vertex_shader_path, GL_VERTEX_SHADER);
	int fragmentShader = ft_loadshader(scop->fragment_shader_path, GL_FRAGMENT_SHADER);

	glAttachShader(scop->glfw->shaderProgram, vertexShader);
	glAttachShader(scop->glfw->shaderProgram, fragmentShader);
	glLinkProgram(scop->glfw->shaderProgram);

	glDetachShader(scop->glfw->shaderProgram, vertexShader);
	glDetachShader(scop->glfw->shaderProgram, fragmentShader);

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
}