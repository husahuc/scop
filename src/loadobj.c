#include "../include/scop.h"

int     ft_error_obj(int error)
{
	if (error == 1)
		ft_putendl("Could not open file obj");
	if (error == 2)
		ft_putendl("Could not load obj");
	return (-1);
}

float   ft_parse_float(char *line)
{
	float nb;
	int ng;
	int f;
	int j;

	nb = 0;
	ng = 0;
	f = 0;
	j = 0;
	if (*line == '-')
		ng = 1;
	if (*line == '-' || *line == '+')
		(line++);
	while ((*line >= '0' && *line <= '9') || *line == '.')
	{
		if (*line == '.')
			f = 1;
		else
		{
			nb = (nb *10) + (*line - '0');
			if (f == 1) {
				j--;
			}
		}
		(line++);
	}
	nb = nb * pow(10, j);
	if (ng == 1)
		nb = -nb;
	return (nb);
}

int   ft_parse_int(char *line)
{
	int nb;
	int ng;

	nb = 0;
	ng = 0;
	if (*line == '-')
		ng = 1;
	if (*line == '-' || *line == '+')
		(line++);
	while (*line >= '0' && *line <= '9')
	{
		nb = (nb *10) + (*line - '0');
		(line++);
	}
	if (ng == 1)
		nb = -nb;
	return (nb);
}

float   *ft_parse(char *line)
{
	float *tab_nb;
	int i;

	i = 0;
	if (!(tab_nb = malloc(3 * sizeof(float))))
		return (NULL);
	while (*line)
	{
		if(*line == ' ')
		{
			(line++);
			tab_nb[i] = ft_parse_float(line);
			i++;
		}
		else
			(line++);
	}
	return (tab_nb);
}

int		ft_count_float(char *line)
{
	int i;
	int nb;

	i = 1;
	nb = 0;
	while (line[i])
	{
		while (line[i] == ' ')
			i++;
		while ((line[i] >= '0' && line[i] <= '9'))
			i++;
		nb++;
	}
	return (nb);
}

int     ft_faces_parse(char *line, t_list **lst_faces, t_scop *scop)
{
	int	*tab_faces;
	int	i;
	int	nb_faces;
	int	*tmp_faces;

	i = 0;
	nb_faces = ft_count_float(line);
	tab_faces = malloc(nb_faces * sizeof(int));
	while (*line)
	{
		if(*line == ' ')
		{
			(line++);
			tab_faces[i] = ft_parse_int(line);
			i++;
		}
		else
			(line++);
	}
	if (nb_faces > 3)
	{
		i = 0;
		while (i < nb_faces - 2)
		{
			tmp_faces = malloc(3 * sizeof(int));
			tmp_faces[0] = tab_faces[0];
			tmp_faces[1] = tab_faces[i+1];
			tmp_faces[2] = tab_faces[i+2];
			ft_lstaddend(lst_faces, tmp_faces);
			scop->obj->nb_faces++;
			i++;
		}
		free(tab_faces);
	}
	else
	{
		ft_lstaddend(lst_faces, tab_faces);
		scop->obj->nb_faces++;
	}
	
	return (1); 
}

int		ft_faces_processing(t_scop *scop, t_list **lst_faces, t_list **vertices)
{
	int		i = 0;
	int		j;
	int		index_vertices = 0;
	int		indice;
	t_list	*tmp_vertice;
	t_list	*tmp_faces;

	scop->obj->vertices = malloc((scop->obj->nb_faces * 3 * 4) * sizeof(float));
	while (i < scop->obj->nb_faces)
	{
		j = 0;
		while (j < 3)
		{
			tmp_faces = ft_lstgetindex(lst_faces, i);
			indice = ((int *)tmp_faces->content)[j] - 1;
			tmp_vertice = ft_lstgetindex(vertices, indice);
			if (tmp_vertice == NULL)
				return (-1);
			scop->obj->vertices[index_vertices++] = ((float *)tmp_vertice->content)[0];
			scop->obj->vertices[index_vertices++] = ((float *)tmp_vertice->content)[1];
			scop->obj->vertices[index_vertices++] = ((float *)tmp_vertice->content)[2];
			scop->obj->vertices[index_vertices++] = (float)i;
			j++;
		}
		i++;
	}
	return (1);
}

int		ft_loadobj(t_scop *scop)
{
	int     	fd;
	char    	*line;
	int     	i;
	float		*tmp_vertice;
	t_list		*lst_vertices;
	t_list		*lst_faces;

	i = 0;
	lst_vertices = NULL;
	lst_faces = NULL;
	if (( fd = open(scop->obj->path, O_RDONLY)) == -1)
		return (ft_error_obj(1));
	while (get_next_line(fd, &line))
	{
		if (line[0] == 'v')
		{
			tmp_vertice = ft_parse(line);
			ft_lstaddend(&lst_vertices, tmp_vertice);
			i++;
		}
		if (line[0] == 'f')
		{
			ft_faces_parse(line, &lst_faces, scop);
		}
		free(line);
	}
	close(fd);
	if (ft_faces_processing(scop, &lst_faces, &lst_vertices) < 0)
		return (ft_error_obj(2));
	ft_lstdel(&lst_vertices, free);
	ft_lstdel(&lst_faces, free);
	return (1);
}