CC = gcc
FLAGS = -Wall -Wextra
RM = rm -f
NAME = scop

SRC_PATH = ./src/
OBJ_PATH = ./obj/

SRC = main.c loadshader.c matrix_math.c glfw.c vector_math.c loadobj.c cli.c loadtexture.c shader.c \
	event.c move_obj.c

OBJ_NAME = $(SRC:.c=.o)
OBJ = $(addprefix $(OBJ_PATH), $(OBJ_NAME))
LIB_FOLDER = lib/
GLFW = $(LIB_FOLDER)glfw-3.3.2.bin.MACOS
LIBFT = $(LIB_FOLDER)mini_libft
FRAMEWORK = -framework Cocoa -framework OpenGL -framework IOKit -framework CoreVideo

all: $(NAME)

$(NAME): $(OBJ) Makefile
	@make -C $(LIBFT)
	$(CC) -o $(NAME) $(OBJ) $(GLFW)/lib-macos/libglfw3.a  $(LIBFT)/libft.a $(FRAMEWORK) -Wno-deprecated-declarations

$(OBJ_PATH)%.o: $(SRC_PATH)%.c $(INC)
	@if test ! -d $(dir $@); then mkdir -p $(dir $@); fi
	$(CC) $(FLAGS) -g -o $@ -c $<

.PHONY: clean fclean re

clean:
	@make clean -C $(LIBFT)
	@$(RM) $(DOBJ)$(OBJ)

fclean: clean
	@make fclean -C $(LIBFT)
	@$(RM) $(NAME)

re: fclean all
