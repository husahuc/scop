#include "libft.h"

void		*ft_memmove(void *dest, const void *src, size_t len)
{
	if (src > dest)
		return (ft_memcpy(dest, src, len));
	while (len--)
		((char*)dest)[len] = ((char*)src)[len];
	return (dest);
}