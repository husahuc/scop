#include "libft.h"

void        ft_lstdel(t_list **lst, void (*del)(void *))
{
    if (lst && *lst)
    {
        if ((*lst)->next)
            ft_lstdel(&(*lst)->next, del);
        del((*lst)->content);
        free(*lst);
        *lst = NULL;
    }
}