/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_memcpy.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: husahuc <marvin@le-101.fr>                   +#+                     */
/*                                                   +#+                      */
/*   Created: 2018/10/02 18:19:31 by husahuc       #+#    #+#                 */
/*   Updated: 2021/06/28 16:55:36 by husahuc       ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */


#include "libft.h"

void	*ft_memcpy(void *str1, const void *str2, size_t n)
{
	char *buf1;
	const char *buf2;
	size_t i = 0;	

	buf1 = (char*)str1;
	buf2 = (const char*)str2;
	while (i++ < n)
	{
		*buf1++ = (char)*buf2++;
	}
	return (str1);
}
