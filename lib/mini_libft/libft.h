/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   libft.h                                            :+:    :+:            */
/*                                                     +:+                    */
/*   By: husahuc <husahuc@student.42.fr>              +#+                     */
/*                                                   +#+                      */
/*   Created: 2018/10/02 15:57:27 by husahuc       #+#    #+#                 */
/*   Updated: 2021/11/12 17:39:02 by husahuc       ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */


#ifndef LIBFT_H
# define LIBFT_H

#include <stddef.h>
#include <unistd.h>
# include <stdarg.h>

# define INFINITE 1

int			ft_atoi(const char *str);
void		ft_putstr(char const *s);
size_t		ft_strlen(const char *str);
void		ft_putnbr(int n);
void		ft_putchar(char c);
void		ft_putnbr_fd(int n, int fd);
void		ft_putchar_fd(char c, int fd);
void		ft_putstr_fd(char const *s, int fd);
int			ft_mini_printf(const char *str, ...);
//void		ft_memdel(void **ap);
void		*ft_memcpy(void *str1, const void *str2, size_t n);
int			ft_strcmp(const char *s1, const char *s2);
char		*ft_strnew(size_t size);
char		*ft_strjoin(char const *s1, char const *s2);
char		*ft_strsub(char const *s, unsigned int start, size_t len);
char		*ft_strdup(const char *src);
void		ft_putendl(char const *s);

int			get_next_line(const int fd, char **line);

typedef struct	s_print {
	char		c;
	void		(*function)(va_list list, int fd);
}				t_print;

# define BUFF_SIZE 600
# include <unistd.h>
# include <stdlib.h>

typedef struct			s_gnl_list
{
	char				*str;
	int					fd;
	struct s_gnl_list	*next;
}						t_gnl_list;

typedef	struct		s_list
{
	void			*content;
	size_t			content_size;
	struct s_list	*next;
}					t_list;


t_list      *ft_lstaddend(t_list **lst, void *content);
void        ft_lstdel(t_list **lst, void (*del)(void *));
t_list      *ft_lstgetindex(t_list **lst, int index);

#endif