#include "libft.h"

t_list      *ft_lstgetindex(t_list **lst, int index)
{
    int     i;
    t_list  *next_lst;

    if (!lst || !*lst)
        return (NULL);

    i = 0;
    next_lst = *lst;
    while (next_lst)
    {
        if (i == index)
            return (next_lst);
        i++;
        next_lst = next_lst->next;
    }
    return (NULL);
}