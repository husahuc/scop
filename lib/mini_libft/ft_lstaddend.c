#include "libft.h"
#include <stdio.h>

t_list      *ft_lstaddend(t_list **lst, void *content)
{
    t_list  *tmp_lst;
    t_list  *next_lst;

    if (content == NULL)
        return (NULL);
    if (lst == NULL)
        return (NULL);
    if (!(tmp_lst = malloc(sizeof(t_list))))
		return (NULL);
    tmp_lst->content = content;
    tmp_lst->next = NULL;
    if (*lst == NULL)
    {
        *lst = tmp_lst;
        return (*lst);
    }
    next_lst = *lst;
    while (next_lst->next != NULL)
    {
        next_lst = next_lst->next;
    }
    next_lst->next = tmp_lst;
    return (next_lst->next);
}