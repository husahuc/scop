/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strdup.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <husahuc@student.42.fr>            +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/03 13:56:28 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2019/02/17 16:44:55 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(const char *src)
{
	size_t	nb;
	char	*dest;
	int		a;

	a = 0;
	if (src == NULL)
		return (NULL);
	nb = ft_strlen(src);
	if (!(dest = (char*)malloc(sizeof(char) * (nb + 1))))
		return (NULL);
	while (src[a] != '\0')
	{
		dest[a] = src[a];
		a++;
	}
	dest[a] = '\0';
	return (dest);
}
